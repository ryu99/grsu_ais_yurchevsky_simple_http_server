package main

import (
	"html/template"
	"net/http"
	"strconv"
)

type Params struct {
	A int64
	B int64
}

func (params *Params) Sum() int64 {
	return params.A + params.B
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/sum", SumHandler)
	server := http.Server{
		Addr:    ":8080",
		Handler: mux,
	}
	server.ListenAndServe()
}

func SumHandler(w http.ResponseWriter, r *http.Request) {
	params := &Params{}
	values := r.URL.Query()
	params.A, _ = strconv.ParseInt(values.Get("a"), 10, 32)
	params.B, _ = strconv.ParseInt(values.Get("b"), 10, 32)

	tmpl, _ := template.ParseFiles("index.html")
	tmpl.Execute(w, params)
}
