FROM golang:1.13
MAINTAINER Yurchevsky Roman <romanyu1999@gmail.com>

WORKDIR /http_server/
COPY . .

EXPOSE 8080

RUN go install

CMD ["sum"]